pub mod newton;
pub mod polynomials;
pub mod root_lists;

use num::Complex;
use newton::newton;
#[allow(unused)]
use polynomials::{
    p_mdlbrt_hyp_cmp, pdp_mdlbrt_hyp_cmp_opt, pqdp_mdlbrt_hyp_cmp,
    p_iterated_quadratic_opt, pdp_iterated_quadratic_opt, pqdp_iterated_quadratic_opt,
    cheb_recursive,
    square_grid_general,
    nroots, dnroots,
    legendre_general,
    laguerre_general,
    hermite_opt, hermite_opt_p, hermite_general,
    rand_on_disk_general
};



fn main() {
    let n = 6;

    // MANDELBROT
    //let deg = (2 as u32).pow((n-1) as u32);
    //let p = |z| p_mdlbrt_hyp_cmp(n, z);
    //let pqdp = |z, q| pqdp_mdlbrt_hyp_cmp(n, z, q);
    //let pdp = |z| pdp_mdlbrt_hyp_cmp_opt(n, z);


    // ITERATED QUADRATIC POLYNOMIAL
    let deg = (2 as u32).pow((n-1) as u32);
    let p = |z| p_iterated_quadratic_opt(n, z);
    let pqdp = |z, q| pqdp_iterated_quadratic_opt(n, z, q);
    let pdp = |z| pdp_iterated_quadratic_opt(n, z);
    
    
    // CHEBYSHEV
    // Half of the radius for every doubling of the degree ~  (deg 20: 0.5i)
    //let deg = n;
    //let p = |z| cheb_recursive(n, z).0;
    //let pqdp = |z, q| { let (c, _, dc, _) = cheb_recursive(n, z); (c-q)/dc };
    //let pdp = |z: Complex<f64>| { let (c, _, dc, _) = cheb_recursive(n, z); c/dc }; 


    // WILKINSON
    //let deg = n;
    //let p = |z| wilkinson_general(n, z).0;
    //let pqdp = |z, q| { let (d, dw) = wilkinson_general(n, z); (d-q)/dw };
    //let pdp = |z: Complex<f64>| { let (d, dw) = wilkinson_general(n, z); d/dw };


    // ROOTS ON SQUARE GRID
    // For starting point approach on a diagonal 
    //let deg = n;
    //let p = |z| square_grid_general(n, z).0;
    //let pqdp = |z, q| { let (s, ds) = square_grid_general(n, z); (s-q) / ds};
    //let pdp = |z| { let (s, ds) = square_grid_general(n, z); s / ds};

    // NROOTS
    // For starting point: the higher the degree, the closer the starting value can be to one
    //let deg = n;
    //let p = |z| nroots(n, z);
    //let pqdp = |z, q| (nroots(n, z) - q) / dnroots(n, z);
    //let pdp = |z: Complex<f64>| nroots(n, z) / dnroots(n, z);

    // LEGENDRE
    // Starting point for legendre can be 1 for all degrees 
    //let deg = n;
    //let p = |z| legendre_general(n, z).0;
    //let pqdp = |z, q| { let (l, _,dl) = legendre_general(n, z); (l-q) / dl };
    //let pdp = |z: Complex<f64>| { let (l, _,dl) = legendre_general(n, z); l / dl }; */

    // LAGUERRE
    // No optimal starting point, but one that works is around the last root on the Real axis
    //let deg = n;
    //let p = |z| laguerre_general(n, z).0;
    //let pqdp = |z, q| { let (l, _,dl) = laguerre_general(n, z); (l-q) / dl };
    //let pdp = |z: Complex<f64>| { let (l, _,dl) = laguerre_general(n, z); l / dl };

    // HERMITE
    //let deg = n;
    //let p = |z| hermite_general(n, z).0;
    //let pqdp = |z, q| { let (h, dh) = hermite_general(n, z); (h-q) / dh };
    //let pdp = |z: Complex<f64>| { let (h, dh) = hermite_general(n, z); h / dh };

    // RANDOM POINTS ON DISK
    //let deg = n;
    //let p = |z| rand_on_disk_general(n, z).0;
    //let pqdp = |z, q| { let (d, dw) = rand_on_disk_general(n, z); (d-q)/dw };
    //let pdp = |z: Complex<f64>| { let (d, dw) = rand_on_disk_general(n, z); d/dw }; */

    //let start_z = Complex { re: -2.0015625, im: 0. };
    // -2.00000015625
    let start_z = Complex { re: -2.0000015625, im: 0.};

    println!("Solving polynomial of degree {}", deg);
    let result = newton(start_z, 1.0e-13, 1.0e-11, deg, &p, &pqdp, &pdp);

    println!("Found {} roots", result.len());

    /* for z in result {
        println!("{:?},", (z.re, z.im))
    }  */
}
