use num::{Complex, complex::ComplexFloat};
use crate::root_lists::{RANDOM_POINTS, SQUARE_GRID};


fn start_mdlbrt(c: Complex<f64>) -> Complex<f64> { c*c + c }
fn start_dmdlbrt(c: Complex<f64>) -> Complex<f64> { 1. + 2.*c }

fn step_mdlbrt(c: Complex<f64>, m: Complex<f64>, large: bool) -> Complex<f64> {
    if large { m*m }
    else { m*m + c }
}
fn step_dmdlbrt(m: Complex<f64>, dm: Complex<f64>, large: bool) -> Complex<f64> {
    if large { 2.*m*dm }
    else { 1. + 2.*m*dm }
}

const LARGE: f64 = 1.0e40;

pub fn pdp_mdlbrt_hyp_cmp_opt(n: u32, c: Complex<f64>) -> Complex<f64> {
    let mut m = start_mdlbrt(c);
    let mut dm = start_dmdlbrt(c);

    let mut large = false;

    for _ in 2..n {
        dm = step_dmdlbrt(m, dm, large);
        m = step_mdlbrt(c, m, large);

        if m.abs() > LARGE && dm.abs() > LARGE {
            large = true;

            m /= LARGE;
            dm /= LARGE;
        }
    }
    m / dm
}

pub fn pqdp_mdlbrt_hyp_cmp(n: u32, c: Complex<f64>, q: Complex<f64>) -> Complex<f64> {
    let mut m = start_mdlbrt(c);
    let mut dm = start_dmdlbrt(c);

    let large = false;

    for _ in 2..n {
        dm = step_dmdlbrt(m, dm, large);
        m = step_mdlbrt(c, m, large);
    }
    (m - q) / dm
}

pub fn p_mdlbrt_hyp_cmp(n: u32, c: Complex<f64>) -> Complex<f64> {
    let mut m = start_mdlbrt(c);

    let large = false;

    for _ in 2..n {
        m = step_mdlbrt(c, m, large);
    }
    m
}


const I: Complex<f64> = Complex { re: 0., im: 1. };

fn start_iterated_quadratic(z: Complex<f64>) -> Complex<f64> { z*z + I }
fn start_diterated_quadratic(z: Complex<f64>) -> Complex<f64> { 2.*z }

fn step_iterated_quadratic(z: Complex<f64>, large: bool) -> Complex<f64> {
    if large { z*z }
    else { z*z + I }
}
fn step_diterated_quadratic(i: Complex<f64>, di: Complex<f64>, large: bool) -> Complex<f64> {
    2.*i*di
}

fn end_iterated_quadratic(i: Complex<f64>, z: Complex<f64>, large: bool) -> Complex<f64> {
    if large { i }
    else { i-z }
}
fn end_diterated_quadratic(di: Complex<f64>, large: bool) -> Complex<f64> {
    if large { di }
    else { di-1. }
}

pub fn pdp_iterated_quadratic_opt(n: u32, z: Complex<f64>) -> Complex<f64> {
    let mut i = start_iterated_quadratic(z);
    let mut di = start_diterated_quadratic(z);

    let mut large = false;

    for _ in 2..n {
        di = step_diterated_quadratic(i, di, large);
        i = step_iterated_quadratic(i, large);

        if i.abs() > LARGE && di.abs() > LARGE {
            large = true;

            i /= LARGE;
            di /= LARGE;
        }
    }
    di = end_diterated_quadratic(di, large);
    i = end_iterated_quadratic(i, z, large);

    i / di
}

pub fn pqdp_iterated_quadratic_opt(n: u32, z: Complex<f64>, q: Complex<f64>) -> Complex<f64> {
    let mut i = start_iterated_quadratic(z);
    let mut di = start_diterated_quadratic(z);

    let large = false;

    for _ in 2..n {
        di = step_diterated_quadratic(i, di, large);
        i = step_iterated_quadratic(i, large);
    }
    di = end_diterated_quadratic(di, large);
    i = end_iterated_quadratic(i, z, large);

    (i-q) / di
}

pub fn p_iterated_quadratic_opt(n: u32, z: Complex<f64>) -> Complex<f64> {
    let mut i = start_iterated_quadratic(z);
 
    let large = false;

    for _ in 2..n {
        i = step_iterated_quadratic(i, large);
    }
    i = end_iterated_quadratic(i, z, large);

    i
}

pub fn cheb_recursive(n: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>, Complex<f64>, Complex<f64>) {
    if n == 0 {
        ((1.).into(), (0.).into(), (0.).into(), (0.).into())
    } else if n == 1 {
        (z, (1.).into(), (1.).into(), (0.).into())
    } else {
        let (
            tn1,
            tn2,
            dtn1,
            dtn2
        ) = cheb_recursive(n-1, z);
        let c = 2.*z*tn1 - tn2;
        let dc = 2.*z*dtn1 + 2.*tn1 - dtn2;
        (c, tn1, dc, dtn1)
    }
}

pub fn wilkinson_general(n: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>) {
    if n == 1 {
        (z - 1., (1.).into())
    } else {
        let (w, dw) = wilkinson_general(n-1, z);
        ((z - n as f64) * w, w + (z - n as f64) * dw)
    }
}

pub fn square_grid_general(n: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>) {
    if n == 1 {
        (z - SQUARE_GRID[n as usize - 1], (1.).into())
    } else {
        let (r, dr) = square_grid_general(n-1, z);
        if r.abs() > LARGE && dr.abs() > LARGE {
            let r = r / LARGE;
            let dr = dr / LARGE;
            ((z - SQUARE_GRID[n as usize - 1]) * r, r + (z - SQUARE_GRID[n as usize - 1]) * dr)
        } else {
            ((z - SQUARE_GRID[n as usize - 1]) * r, r + (z - SQUARE_GRID[n as usize - 1]) * dr)
        }
    }
}

pub fn nroots(n: u32, z: Complex<f64>) -> Complex<f64> {
    z.powu(n) - 1.
}

pub fn dnroots(n: u32, z: Complex<f64>) -> Complex<f64> {
    n as f64 * z.powu(n-1)
}


pub fn legendre_general(k: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>, Complex<f64>) {
    if k == 1 {
        (z, (1.).into(), (1.).into())
    } else {
        let (l1, l2, dl1) = legendre_general(k-1, z);
        (((2*k-1) as f64 * z * l1 - (k-1) as f64 * l2) / k as f64, l1, k as f64 * l1 + z * dl1)
    }
}

pub fn laguerre_general(k: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>, Complex<f64>) {
    if k == 1 {
        (1. - z, (1.).into(), (-1.).into())
    } else {
        let (l1, l2, dl1) = laguerre_general(k-1, z);
        (((2.*(k-1) as f64 + 1. - z) * l1 - (k-1) as f64 * l2) / k as f64, l1, dl1 - l1)
    }
}

fn hermite_opt_proto(n: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>) {
    if n == 1 {
        (2.*z, (1.).into())
    } else {
        let (h1, h2) = hermite_opt_proto(n-1, z);
        (2. * (z * h1 - (n-1) as f64 * h2), h1)
    }
}

pub fn hermite_opt(n: u32, z: Complex<f64>) -> Complex<f64> {
    let (h1, h2) = hermite_opt_proto(n-1, z);
    let mut frac = h2 / h1;
    if frac.is_nan() {  // Apparently division of Complex numbers larger than ca. 1.0e150 by each other results in NaN
        frac = (h2 * 1.0e-100) / (h1 * 1.0e-100)
    }
    z / (n as f64) - ((n - 1) as f64 / n as f64) * frac
}

pub fn hermite_opt_p(n: u32, z: Complex<f64>) -> Complex<f64> { 
    let (h1, h2) = hermite_opt_proto(n-1, z);
    2. * (z * h1 - (n-1) as f64 * h2)
}

pub fn hermite_general(n: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>) {
    if n == 1 {
        (2.*z, (2.).into())
    } else {
        let (h, dh) = hermite_general(n-1, z);
       /*  if (h / dh).is_nan() {
            dbg!(h / dh);
            dbg!(h);
            dbg!(dh);
            dbg!(n);
            //panic!();
        } */
        (2. * z * h - dh, 2. * n as f64 * h)
    }
}

pub fn rand_on_disk_general(n: u32, z: Complex<f64>) -> (Complex<f64>, Complex<f64>) {
    if n == 1 {
            (z - RANDOM_POINTS[n as usize - 1], (1.).into())
    } else {
            let (r, dr) = rand_on_disk_general(n-1, z);
            ((z - RANDOM_POINTS[n as usize - 1]) * r, r + (z - RANDOM_POINTS[n as usize - 1]) * dr)
    }
}
    

