use std::time::Instant;
use std::f64::consts;
use num::{Complex, complex::ComplexFloat};
#[allow(unused)]
use std::fs::OpenOptions;
#[allow(unused)]
use std::io::prelude::*;


pub fn newton(start_z: Complex<f64>, eps_stop: f64, eps_root: f64, deg: u32,
    p: &impl Fn(Complex<f64>) -> Complex<f64>,
    pqdp: &impl Fn(Complex<f64>, Complex<f64>) -> Complex<f64>,
    pdp: &impl Fn(Complex<f64>) -> Complex<f64>) -> Vec<Complex<f64>> {
    // Start time for start orbit placing
    let now = Instant::now();

    let mut newton_iterations = 0;
    
    let mut start_orbits = vec![start_z];

    let mut pz = p(start_z).to_polar();
    newton_iterations += 1;
    
    // Set general radius of how close the curve around all roots is going to be calculated
    let r = pz.0;
    println!("Radius of size {}", r);

    // How much each new starting position is going to be offset from the previous
    let delta_phi = 2.*consts::PI / 4.;

    for i in 1..deg*4 {
        if i % 20000 == 0 {
            println!("r: {}, theta: {}", pz.0, pz.1);
        }
        let q = Complex::from_polar(r, pz.1 + delta_phi);
        let mut z = start_orbits.last().expect("This shouldn't be an empty vector").clone();

        // Via newton iteration find new start orbit `z`
        // TODO: check how many iterations are really needed or if a variable should be used
        for _ in 0..6 {
            z = z - pqdp(z, q);
        }
        newton_iterations += 6;

        start_orbits.push(z);
        pz = p(z).to_polar();
        newton_iterations += 1;
    }

    // Get time for start orbit placing
    let elapsed_time = now.elapsed();
    println!("Orbit placing: {}ms", elapsed_time.as_millis());

    // OUTPUT FOR START ORBITS
    /* let _f = OpenOptions::new().create(true).write(true).truncate(true).open("orbits.txt").expect("initializing file didn't work");
    let mut file = OpenOptions::new().append(true).open("orbits.txt").expect("opening file didn't work");

    file.write_fmt(format_args!("{:?},\n", complex_vector_to_tuples(&start_orbits))).expect("writing to file didn't work"); */
    // OUTPUT FOR START ORBITS END

    // Start usual newton iteration
    let max_iterations = 5 * deg;

    let mut z_converged = vec![];
    let mut z_n = start_orbits;
    let mut pdp_n: Vec<Complex<f64>>;

    let mut z_temp: Vec<Complex<f64>>;

    let mut z_len = z_n.len();

    let mut iteration = 0;
    while max_iterations > iteration && z_len != 0 {
        // New iteration
        iteration += 1;

        z_len = z_n.len();

        // ---UPDATE--- p_n
        pdp_n = z_n.iter().map(|z| pdp(*z)).collect();
        newton_iterations += z_len;

        // Make one newton iteration
        // ---UPDATE--- z_n
        z_n = z_n.iter().enumerate().map(|(i, z)| z - pdp_n[i]).collect();


        // reset `z_temp`
        z_temp = vec![];

        for (i, z) in z_n.iter().enumerate() {
            if pdp_n[i].abs() < eps_stop {
                z_converged.push(*z);
            } else {
                z_temp.push(*z)
            }
        }

        // ---UPDATE--- z_n
        z_n = z_temp
    }

    dbg!(z_n.len());
    dbg!(z_converged.len());
    dbg!(iteration);

    println!("{} iteration steps", newton_iterations);

    post_processing_roots(z_converged, eps_root)
}



fn post_processing_roots(mut z_converged: Vec<Complex<f64>>, eps_root: f64) -> Vec<Complex<f64>> {
    z_converged.sort_by(|a: &Complex<f64>, b: &Complex<f64>| a.re.partial_cmp(&b.re).unwrap());

    let mut distinct_roots = vec![];
    distinct_roots.push(*z_converged.first().unwrap());

    for z in z_converged {
        let mut already_distinct = false;
        for c in distinct_roots.iter().rev() {
            if z.re - c.re < eps_root {
                if (z - c).abs() < eps_root {
                    already_distinct = true;
                    break;
                }
            } else {
                break;
            }
        }

        if !already_distinct {
            distinct_roots.push(z);
        }
    }
    distinct_roots
}


#[allow(unused)]
fn complex_vector_to_tuples(zs: &Vec<Complex<f64>>) -> Vec<(f64, f64)> {
    zs.iter().map(|z| (z.re(), z.im())).collect()
}